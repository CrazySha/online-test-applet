// pages/answerInfo/index.js
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    time: '',
    questionMenu: '',
    questionNum: '',
    jige: '',
    isLock: '',
    isScore: '',
    infomessage: "本次考试不加密！",
    suijitimu:"本次考试题目所有考生全部一致！",
    spinShow: false,
    userpassword: null,
    password: null,
    isPassword: "否"
  },
  onLoad(e) {
    var id = e.id
    let _this = this

    _this.setData({
      spinShow: true
    });

    app.formPost('/api/wx/student/exampaper/select/' + id, null)
      .then(res => {
        if (res.code === 1) {
          _this.setData({
            spinShow: false,
            id: res.response.id,
            questionMenu: res.response.name,
            questionNum: res.response.score,
            time: res.response.suggestTime,
            isPassword: res.response.isPassword,
            password: res.response.password,
            jige: (res.response.score * 0.6).toFixed(),
          });
          app.globalData.password=res.response.password
          app.globalData.doExamPaperId=res.response.id
          if (res.response.isPassword == "是") {
            _this.setData({
              infomessage: "本次考试为加密考试，需输入密码！"
            })
          }
          if (res.response.isSuiji == "0") {
            _this.setData({
              suijitimu: "本次考试所有考生题目均从题库随机抽取，考试题目完全随机！"
            })
          }
          if (res.response.isLock == "是") {
            _this.setData({
              isLock: "公开"
            })
          } else {
            _this.setData({
              isLock: "不公开"
            });
          }
          if (res.response.isScore == "是") {
            _this.setData({
              isScore: "公开"
            })
          } else {
            _this.setData({
              isScore: "不公开"
            })
          };
        };
      })
  },
  start() {
    wx.reLaunch({
      url: '/pages/exam/do/index?id=' + this.data.id
    })
  },
  showModal(e) {
    this.setData({
      modalName: "DialogModal1"
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  beforeStart() {
    let that = this
    console.log("原始密码" + that.data.password)
    console.log("输入密码" + that.data.userpassword)
    if (that.data.password == that.data.userpassword) {
      wx.reLaunch({
        url: '/pages/exam/do/index?id=' + that.data.id
      })
    } else {
      // app.message("密码输入错误！", 'error')
      console.log("密码输入错误！")
      wx.showModal({
        title: '提示',
        content: '试卷密码输入错误！'
      })
    }
  },
  toStart() {
    let that = this
    if (that.data.isPassword == "是") {
      // that.setData({
      //   modalName: "DialogModal1"
      // }) 

      wx.navigateTo({
        url: '/pages/lockscreen/lockscreen?id=' + that.data.id
      })

    } else {
      wx.reLaunch({
        url: '/pages/exam/do/index?id=' + that.data.id
      })
    }
  },
  getInputValue(e) {
    let that = this
    that.setData({
      userpassword: e.detail.value
    })
    console.log(e.detail.value) // {value: "ff", cursor: 2}  
  }
})