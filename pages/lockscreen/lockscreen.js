const util = require('../../utils/password.js')
let app = getApp()
Page({
  data: {
    numberArr: [],
    pwdArr: ["", "", "", "", "", ""]
  },
  onLoad: function(options) {

  },
  closeKeyboard: function() {
    this.setData({
      numberArr: [],
      pwdArr: ["", "", "", "", "", ""]
    })
  },
  goIndex: function() {
   wx.reLaunch({
     url: '../../pages/index/index',
   })
  },
  getPwd: function() {
    //判断并取出密码
    if (this.data.numberArr.length === this.data.pwdArr.length) {
      let pwd = this.data.numberArr.join('')
      if (pwd == app.globalData.password) {
       
        wx.navigateTo({
          url: '/pages/exam/do/index?id=' + app.globalData.doExamPaperId
        })
      }else{
        wx.showModal({
          title: '提示',
          content: '密码错误！'
        })
        this.closeKeyboard();
      }
    }
  },
  keyboardClick: function(e) {
    let numberArr = this.data.numberArr;
    let pwdArr = this.data.pwdArr;
    let index = e.detail.index;
    if (numberArr.length === pwdArr.length || index == undefined) {
      return;
    }
    if (index == 9) { //取消键
      this.closeKeyboard();
      return;
    } else if (index == 11) {
      //退格键
      let len = numberArr.length;
      if (len) {
        pwdArr.splice(len - 1, 1, "");
      } else {
        pwdArr = ["", "", "", ""];
      }
      numberArr.pop()
    } else if (index == 10) {
      numberArr.push(0);
      pwdArr.splice(numberArr.length - 1, 1, "●");
    } else {
      numberArr.push(index + 1);
      pwdArr.splice(numberArr.length - 1, 1, "●");
    }
    this.setData({
      numberArr: numberArr,
      pwdArr: pwdArr
    }, () => {
      this.getPwd()
    })
  }
})