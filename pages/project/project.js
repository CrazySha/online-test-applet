// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,
    userInfo: null,
    modalName:null,
    neirong:null,
    queryParam: {
      pageIndex: "1",
      pageSize: "100",
      name:"漏损控制"
    },
    queryParam1: {
      pageIndex: "1",
      pageSize: "100",
      name:"二供平台"
    },
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [], 
    certList1: [], 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
       this.setData({
        certList:res.response.list,
        userInfo: app.globalData.userInfo
       })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam1)
    .then(res => {
      if (res.code == 1) {
       this.setData({
        certList1:res.response.list,
       })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
    this.inital();
  },
  inital: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
      that.setData({
        modalName:"Modal",
        neirong: url
      });
      wx.setClipboardData({
        data: url,
        success(res) {
          wx.getClipboardData({
            success(res) {
              console.log(res.data) // data
              wx.showToast({
                title: '复制成功,粘贴到浏览器访问',
                icon: 'none'
              })
            }
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 优秀方案',
      path: 'pages/project/project'
    }
  },
})