const app = getApp()
Page({
  data: {
    levelIndex: 0,
    deptIndex: 0,
    spinShow: false
  },
  bindLevelChange: function (e) {
    this.setData({
      levelIndex: e.detail.value,
    })
  },
  bindDeptChange: function (e) {
    this.setData({
      deptIndex: e.detail.value,
    })
  },
  formSubmit: function (e) {
    let _this = this;
    let form = e.detail.value
    console.log(form)
    if (form.userName == null || form.userName == '') {
      wx.showModal({
        title: '用户名不能为空'
      })
      return;
    }
    if (form.realName == null || form.realName == '') {
      wx.showModal({
        title: '真实姓名不能为空'
      })
      return;
    }
    if (form.password == null || form.password == '') {
      wx.showModal({
        title: '密码不能为空'
      })
      return;
    }
    // if (form.userLevel == null) {
    //   wx.showModal({
    //     title: '分公司不能为空'
    //   })
    //   return;
    // }
    // if (form.useDept == null) {
    //   wx.showModal({
    //     title: '部门不能为空'
    //   })
    //   return;
    // }
    // this.zhToPinYin = new ZhToPinYin.default(false);
    // const newUserName=this.zhToPinYin.getPinYin(formNew.userName, false)
    // console.log(newUserName)
    _this.setData({
      spinShow: true
    });
    
    app.formPost('/api/wx/student/user/register', form)
      .then(res => {
        _this.setData({
          spinShow: false
        });
        if (res.code == 1) {
          wx.reLaunch({
            url: '/pages/user/bind/index',
          });
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        wx.showModal({
          title: e
        })
      })
  }
})