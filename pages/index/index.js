const app = getApp();
Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        cardCur: 0,
        spinShow: false,
        fixedPaperCount: 0,
        taskPaperCount: 0,
        timeLimitPaperCount: 0,
        pushPaperCount: 0,
        fixedPaper: [],
        pushPaper: [],
        timeLimitPaper: [],
        taskList: [],
        msgList: [],
        userInfo: null,
        greetOne:null
    },
    pageLifetimes: {
        show() {
            if (typeof this.getTabBar === 'function' && this.getTabBar()) {
                this.getTabBar().setData({
                    selected: 0
                })
            }
        }
    },
    onLoad: function () {
        this.setData({
          spinShow: true
        });
        this.indexLoad()
        this.Get_time()
        app.formPost('/api/wx/student/user/current', null).then(res => {
          if (res.code == 1) {
            app.globalData.userInfo = res.response
            this.setData({
              userInfo: res.response
            });
          }
          
        }).catch(e => {
          app.message(e, 'error')
        })
      },
      Get_time() {
        var that = this
        /* 加载时间日期 */
        var myDate = new Date();
        var month = myDate.getMonth() + 1
        var date = month + "-" + myDate.getDate();
        var h = myDate.getHours();       //获取当前小时数(0-23)
        if (h < 10) {
          h = "0" + String(h)
        }
        var greet = '';
        if (h < 6) {
          greet = "哎，失眠了"
        } else if (h < 9) {
          greet = "Hi，早上好吖"
        } else if (h < 12) {
          greet = "Hi，中午好吖"
        } else if (h < 14) {
          greet = "Hi，下午好吖"
        } else if (h < 17) {
          greet = "Hi，快下班吖"
        } else if (h < 21) {
          greet = "Hi，早点休息吖"
        } else if (h < 23) {
          greet = "Hi，洗洗睡吖"
        } else if (h = 23) {
          greet = "Hi，晚安吖"
        }
        this.setData({
          greetOne: greet
        });
    },
      indexLoad: function () {
        let _this = this
        app.formPost('/api/wx/student/dashboard/index', null).then(res => {
          _this.setData({
            spinShow: false
          });
          wx.stopPullDownRefresh()
          if (res.code === 1) {
            _this.setData({
              fixedPaper: res.response.fixedPaper,
              timeLimitPaper: res.response.timeLimitPaper,
              pushPaper: res.response.pushPaper,
              msgList: res.response.msgList,
              fixedPaperCount: res.response.fixedPaper.length,
              timeLimitPaperCount: res.response.timeLimitPaper.length
            });
            app.globalData.fixedPaper= res.response.fixedPaper
            app.globalData.timeLimitPaper= res.response.timeLimitPaper
            app.globalData.pushPaper= res.response.pushPaper
            app.globalData.fixedPaperCount= res.response.fixedPaper.length,
            app.globalData.timeLimitPaperCount= res.response.timeLimitPaper.length
            if( res.response.pushPaper!=null){
              _this.setData({
                pushPaperCount: res.response.pushPaper.length
              })
              app.globalData.pushPaperCount= res.response.pushPaper.length
            }
           
           
          }
        }).catch(e => {
          _this.setData({
            spinShow: false
          });
          // app.message(e, 'error')
        })
    
        app.formPost('/api/wx/student/dashboard/task', null).then(res => {
          _this.setData({
            spinShow: false
          });
          wx.stopPullDownRefresh()
          if (res.code === 1) {
            _this.setData({
              taskList: res.response,
              taskPaperCount: res.response.length
            });
            app.globalData.taskList= res.response
            app.globalData.taskPaperCount= res.response.length
          }
        }).catch(e => {
          _this.setData({
            spinShow: false
          });
          // app.message(e, 'error')
        })
      },

      onShareAppMessage: function (res) {
        if (res.from === 'button') {
          // 来自页面内转发按钮
          console.log(res.target)
        }
        return {
          title: '在线考试exam',
          path: '/page/index/index'
        }
      },
      onShareTimeline: function (t) {
        return {
          title: "在线考试exam",
          query: "",
          imageUrl: ""
        }
      },
      onPullDownRefresh() {
        this.setData({
          spinShow: true
        });
        if (!this.loading) {
          this.indexLoad()
        }
      },

        // 跳转到业务详情
        navProcess(e) {
            wx.navigateTo({
                url: '/pages/book/book',
            });
        },
        navProcess1(e) {
          // wx.navigateTo({
          //     url: '/pages/introduce/index',
          // });
          wx.navigateToMiniProgram({
            appId: 'wxb7c9e6814c98ac52',
            path:'pages/index/index',
            extraData:{
              foo:'bar'
            },
            envVersion:'develop',
            success(res){
              
            }
          })
      },
      navProcess2(e) {
        wx.navigateTo({
            url: '/pages/exam/index/index',
        });
    },
    navProcess3(e) {
      wx.reLaunch({
        url: '/pages/examPapers/index',
      });
    },
    navProcess4(e) {
      wx.navigateTo({
        url: '/pages/web/web',
      });
    },
    navProcess5(e) {
      wx.navigateTo({
        url: '/pages/cert/cert',
      });
    },
    navProcess6(e) {
      wx.navigateTo({
        url: '/pages/search/search',
      });
    },
    navProcess7(e) {
      wx.navigateTo({
        url: '/pages/wenDa/wenDa',
      });
    },
    navProcess8(e) {
      wx.navigateTo({
        url: '/pages/user/all/all',
      });
    },
    navProcess9(e) {
      wx.navigateTo({
        url: '/pages/project/project',
      });
    },
      changyong(e) {
        wx.reLaunch({
          url: '/pages/home/index',
        })
  },
  quanbu(e) {
    wx.reLaunch({
      url: '/pages/examPapers/index',
    })
},
        // 跳转到案例详情
        navDetail(e) {
            wx.navigateTo({
                url: '../detail/detail',
            });
        },
        // 跳转到搜索
        navSearch(e) {
            wx.navigateTo({
                url: '../search/search',
            });
        },
})