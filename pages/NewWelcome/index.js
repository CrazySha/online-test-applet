const app = getApp();
const amapFile = require('../../utils/amap-wx.130');
Page({
  data: {
    rain_bg: 'http://download.tpengyun.cn/res/WeatherTop/rain_background.jpg', //下雨背景
    snow_bg: 'http://download.tpengyun.cn/res/WeatherTop/snow_background.jpg', //下雪背景
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    ColorList: app.globalData.ColorList,
    isLoading:false,
    weather: 2, //1为下雨 2为下雪
    City: '',
    wind: '',
    Desc:'',
    temperature: '',
    spinShow: false,
    recruits: [{
      title: '立即进入',
      introduce: '立即进入在线考试系统！',
      bgColor: 'bg-blue',
      bgImgUrl: 'http://img.uelink.com.cn/upload/xykj/bg_eval.png',
      url: 'eval/index'
    },
    {
      title: '订阅成绩',
      introduce: '订阅考试成绩以及考试通知！',
      bgColor: 'bg-green',
      bgImgUrl: 'http://img.uelink.com.cn/upload/xykj/bg_race.png',
      url: 'race/index'
    }
  ]
  },
  onLoad: function () {

    var that = this;
    that.setData({
      spinShow: true
    });
    var myAmapFun = new amapFile.AMapWX({
      key: 'fdbdc5e5e590469e347e5898fd259235'
    });
    that.setData({
      spinShow: false
    });
    myAmapFun.getWeather({
      success: function (data) {
        console.log('数据', data)
        //成功回调
        that.setData({
          City: data.city.data, //城市
          pm25: data.humidity.data, //湿度
          Desc: data.liveData.weather, //天气
          wind: data.winddirection.data, //风力
          temperature: data.temperature.data, //温度
          // date: weather_data.date, //日期
          // tips: data.index[0].des, //穿衣提醒
        });
        console.log(that.data.Desc)
        var string = that.data.Desc;
        if (string.indexOf("雨") >= 0 || string.indexOf("阴") >= 0) {
          that.setData({
            weather: 1,
          });
        }
        if (string.indexOf("雪") >= 0 || string.indexOf("晴") >= 0) {
          that.setData({
            weather: 2,
          });
        }
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },
  subMsg() {
    wx.requestSubscribeMessage({
      tmplIds: ['Q5B4N5l0cmyoNYC7siPyF7rv-xN6aT8FV3jWqUCRQ2Y', 'MPdYYz2g1BlAWRyg9bUOiOZwS6Q0l_1Ag48OyM6F1bs', 'Ys-DSceVKE9EdxlkZgUceNNbikobYAntZ_weRNvG0qE'],
      success(res) {
        console.log("授权成功！")
        wx.showToast({
          title: '订阅成功！',
          icon: 'success',
          duration: 2000 //持续的时间
        })
        wx.vibrateShort({
          type: 'heavy'
        })
      },
      fail(res) {
        wx.showToast({
          title: '订阅失败！！！',
          icon: 'error',
          duration: 2000 //持续的时间
        })
      }
    })
  },
  goSign() {
    var that = this;
    wx.setStorageSync('isFirst', true)
    that.setData({
      isLoading: true
    });
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 3000);
    wx.reLaunch({
      url: '../../pages/index/index',
    })
  },
  /**
   * 切换天气
   */
  toDo(e) {
    console.log(e)
    this.setData({
      weather: e.currentTarget.dataset.weather
    })
  }
})