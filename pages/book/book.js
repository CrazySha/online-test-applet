// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,
    userInfo: null,
    modalName:null,
    neirong:null,
    hasWeappMethod: ['http://cet.neea.edu.cn/cet/'],
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [{
        title: '漏损事业部赋能培训材料V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%BC%8F%E6%8D%9F%E4%BA%8B%E4%B8%9A%E9%83%A8%E8%B5%8B%E8%83%BD%E5%9F%B9%E8%AE%AD%E6%9D%90%E6%96%99V1.0.pdf"
      }, {
        title: '漏损解决方案与和达案竞争分析（20210428）V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%BC%8F%E6%8D%9F%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88%E4%B8%8E%E5%92%8C%E8%BE%BE%E6%A1%88%E7%AB%9E%E4%BA%89%E5%88%86%E6%9E%90%EF%BC%8820210428%EF%BC%89.pdf"
      },
      {
        title: '水联网具体案例介绍V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%B0%B4%E8%81%94%E7%BD%91%E5%85%B7%E4%BD%93%E6%A1%88%E4%BE%8B%E4%BB%8B%E7%BB%8D.pdf"
      },
      {
        title: 'leakview推荐服务器配置V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%8E%A8%E8%8D%90%E6%9C%8D%E5%8A%A1%E5%99%A8%E9%85%8D%E7%BD%AE.pdf"
      },
      {
        title: '城镇供水管网分区计量管理工作指南',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E5%9F%8E%E9%95%87%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%BD%91%E5%88%86%E5%8C%BA%E8%AE%A1%E9%87%8F%E7%AE%A1%E7%90%86%E5%B7%A5%E4%BD%9C%E6%8C%87%E5%8D%97.pdf"
      },
      {
        title: 'CJJ 92-2016 城镇供水管网漏损控制及评定标准(电子版)',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/CJJ%2092-2016%20%E5%9F%8E%E9%95%87%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%BD%91%E6%BC%8F%E6%8D%9F%E6%8E%A7%E5%88%B6%E5%8F%8A%E8%AF%84%E5%AE%9A%E6%A0%87%E5%87%86(%E7%94%B5%E5%AD%90%E7%89%88).pdf"
      },
      {
        title: '城镇水务全流程环节基础知识培训v1.2',
        hookId: 'pptx',
        url: "http://106.14.66.250:9000/dma/%E5%9F%8E%E9%95%87%E6%B0%B4%E5%8A%A1%E5%85%A8%E6%B5%81%E7%A8%8B%E7%8E%AF%E8%8A%82%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E5%9F%B9%E8%AE%ADv1.2.pptx"
      },
      {
        title: '无收益水量管理手册',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%97%A0%E6%94%B6%E7%9B%8A%E6%B0%B4%E9%87%8F%E7%AE%A1%E7%90%86%E6%89%8B%E5%86%8C.pdf"
      },
      {
        title: '分区计量漏损管理解决方案产品（硬件）',
        hookId: 'pptx',
        url: "http://106.14.66.250:9000/dma/%E5%88%86%E5%8C%BA%E8%AE%A1%E9%87%8F%E6%BC%8F%E6%8D%9F%E7%AE%A1%E7%90%86%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88%E4%BA%A7%E5%93%81%EF%BC%88%E7%A1%AC%E4%BB%B6%EF%BC%89.pptx"
      },
      {
        title: '威傲水表选型与说明2020（培训版）',
        hookId: 'xlsx',
        url: "http://106.14.66.250:9000/dma/%E5%A8%81%E5%82%B2%20%E6%B0%B4%E8%A1%A8%E9%80%89%E5%9E%8B%E4%B8%8E%E8%AF%B4%E6%98%8E2020%EF%BC%88%E5%9F%B9%E8%AE%AD%E7%89%88%EF%BC%89.xlsx"
      },
    ],
    certList1: [{
        title: '检漏设备V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%A3%80%E6%BC%8F%E8%AE%BE%E5%A4%87.pdf"
      }, {
        title: 'ChronoFLO2时差法超声波管道流量计V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/ChronoFLO2%E6%97%B6%E5%B7%AE%E6%B3%95%E8%B6%85%E5%A3%B0%E6%B3%A2%E7%AE%A1%E9%81%93%E6%B5%81%E9%87%8F%E8%AE%A1.pdf"
      },
      {
        title: 'HydrINS2插入式电磁流量计V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/HydrINS2%E6%8F%92%E5%85%A5%E5%BC%8F%E7%94%B5%E7%A3%81%E6%B5%81%E9%87%8F%E8%AE%A1.pdf"
      },
      {
        title: '低功耗485压力传感器V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E4%BD%8E%E5%8A%9F%E8%80%97485%E5%8E%8B%E5%8A%9B%E4%BC%A0%E6%84%9F%E5%99%A8.pdf"
      },
      {
        title: '水听器V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%B0%B4%E5%90%AC%E5%99%A8%E5%BD%A9%E9%A1%B5.pdf"
      },
      {
        title: 'PermaNet+噪声记录仪V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/PermaNet+%E5%99%AA%E5%A3%B0%E8%AE%B0%E5%BD%95%E4%BB%AA.pdf"
      },
      {
        title: '部分波纹不锈钢波状管-1V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E9%83%A8%E5%88%86%E6%B3%A2%E7%BA%B9%E4%B8%8D%E9%94%88%E9%92%A2%E6%B3%A2%E7%8A%B6%E7%AE%A1%E5%BD%A9%E9%A1%B5.pdf"
      },
      {
        title: '部分波纹不锈钢波状管-2V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E9%83%A8%E5%88%86%E6%B3%A2%E7%BA%B9%E4%B8%8D%E9%94%88%E9%92%A2%E6%B3%A2%E7%8A%B6%E7%AE%A1%E5%BD%A9%E9%A1%B5-1.pdf"
      },
      {
        title: 'RPS泄压阀V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/RPS%E6%B3%84%E5%8E%8B%E9%98%80.pdf"
      },
      {
        title: 'PR水力减压阀V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/PR%E6%B0%B4%E5%8A%9B%E5%87%8F%E5%8E%8B%E9%98%80.pdf"
      },
      {
        title: 'Pegasus+压力控制器V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/Pegasus+%E5%8E%8B%E5%8A%9B%E6%8E%A7%E5%88%B6%E5%99%A8.pdf"
      },
      {
        title: 'NB-IOT智能井盖V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/NB-IOT%E6%99%BA%E8%83%BD%E4%BA%95%E7%9B%96.pdf"
      },
      {
        title: 'permanet SUV1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/permanet%20SU%E5%BD%A9%E9%A1%B5.pdf"
      },
      {
        title: 'PCorr+智能型噪声记录仪V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/PCorr+%E6%99%BA%E8%83%BD%E5%9E%8B%E5%99%AA%E5%A3%B0%E8%AE%B0%E5%BD%95%E4%BB%AA%E5%BD%A9%E9%A1%B5(1).pdf"
      },
      {
        title: '水联网远传数据终端-4GV1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E2%98%85%E6%B0%B4%E8%81%94%E7%BD%91%E8%BF%9C%E4%BC%A0%E6%95%B0%E6%8D%AE%E7%BB%88%E7%AB%AF%E5%8D%95%E9%A1%B5-4G-20200813.pdf"
      },
    ],
    certList2: [{
        title: '智慧水务-错峰供水-方案V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E6%B0%B4%E5%8A%A1-%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4-%E6%96%B9%E6%A1%88.pdf"
      }, {
        title: '智慧水务-错峰供水-PPTV1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E6%B0%B4%E5%8A%A1-%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4-PPT.pdf"
      },
      {
        title: '武清错峰供水调蓄效果报告V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%AD%A6%E6%B8%85%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4%E8%B0%83%E8%93%84%E6%95%88%E6%9E%9C%E6%8A%A5%E5%91%8A.pdf"
      },
      {
        title: '沧州错峰供水案例分享V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%B2%A7%E5%B7%9E%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4%E6%A1%88%E4%BE%8B%E5%88%86%E4%BA%AB.pdf"
      },
      {
        title: '智慧水务-错峰供水-用户验收单V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E6%B0%B4%E5%8A%A1-%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4-%E7%94%A8%E6%88%B7%E9%AA%8C%E6%94%B6%E5%8D%95.pdf"
      },
      {
        title: '长沙错峰调蓄答疑（务虚版本）V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E9%95%BF%E6%B2%99%E9%94%99%E5%B3%B0%E8%B0%83%E8%93%84%E7%AD%94%E7%96%91%EF%BC%88%E5%8A%A1%E8%99%9A%E7%89%88%E6%9C%AC%EF%BC%89.pdf"
      },
      {
        title: '错峰供水原理（务实版本）V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4%E5%8E%9F%E7%90%86%EF%BC%88%E5%8A%A1%E5%AE%9E%E7%89%88%E6%9C%AC%EF%BC%89.pdf"
      },
      {
        title: '智慧水务-错峰供水-硬件、软件报价清单V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E6%B0%B4%E5%8A%A1-%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4-%E7%A1%AC%E4%BB%B6%E3%80%81%E8%BD%AF%E4%BB%B6%E6%8A%A5%E4%BB%B7%E6%B8%85%E5%8D%95.pdf"
      }
    ],
    certListOne: [{
      title: 'AR客户商机调研表V1.0(漏损)',
      hookId: 'xlsx',
      url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/AR%E5%AE%A2%E6%88%B7%E5%95%86%E6%9C%BA%E8%B0%83%E7%A0%94%E8%A1%A8.xlsx?sign=6d6ac5b2f689387f941c284028481ccf&t=1621237615"
    }, {
      title: 'SR客户商机调研表V1.0(漏损)',
      hookId: 'xlsx',
      url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/05%E6%B0%B4%E8%81%94%E7%BD%91%E6%B0%B4%E5%8F%B8%E6%B7%B1%E5%BA%A6%E8%B0%83%E7%A0%94%E8%A1%A8.xlsx?sign=855c4c41763cea2be14afdd8a85b579d&t=1621237644"
    }],
    certListOne1: [
      {
        title: '智慧水务-错峰供水-基础调研表V1.0',
        hookId: 'pdf',
        url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E6%B0%B4%E5%8A%A1-%E9%94%99%E5%B3%B0%E4%BE%9B%E6%B0%B4-%E5%9F%BA%E7%A1%80%E8%B0%83%E7%A0%94%E8%A1%A8.pdf"
      }],
      certList3: [
        {
          title: '网络基础知识培训v0.2',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E7%BD%91%E7%BB%9C%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E5%9F%B9%E8%AE%AD-V0.2.pptx"
        },
        {
          title: '如何写好一个解决方案',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E5%A6%82%E4%BD%95%E5%86%99%E5%A5%BD%E4%B8%80%E4%B8%AA%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88.pptx"
        },
        {
          title: '移动通信与软件基础知识培训v1.4简略版',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E7%A7%BB%E5%8A%A8%E9%80%9A%E4%BF%A1%E4%B8%8E%E8%BD%AF%E4%BB%B6%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E5%9F%B9%E8%AE%ADv1.4%E7%AE%80%E7%95%A5%E7%89%88.pptx"
        },
      ],
      certList4: [
        {
          title: '智慧供水管理平台培训v3.0',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%90%86%E5%B9%B3%E5%8F%B0%E5%9F%B9%E8%AE%ADv3.0.pptx"
        },
        {
          title: '智慧供水管理平台交流会2021版本v3.5',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E6%99%BA%E6%85%A7%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%90%86%E5%B9%B3%E5%8F%B0%E4%BA%A4%E6%B5%81%E4%BC%9A2021%E7%89%88%E6%9C%ACv3.5.pptx"
        },
        {
          title: 'WPG智慧供水管理平台差异化PPT',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/WPG%E6%99%BA%E6%85%A7%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%90%86%E5%B9%B3%E5%8F%B0%E5%B7%AE%E5%BC%82%E5%8C%96PPT.pptx"
        },
        {
          title: '威派格硬件介绍-2021版本',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/%E5%A8%81%E6%B4%BE%E6%A0%BC%E7%A1%AC%E4%BB%B6%E4%BB%8B%E7%BB%8D-2021%E7%89%88%E6%9C%AC.pptx"
        },
        {
          title: 'WPG智慧供水管理平台技术培训',
          hookId: 'pptx',
          url: "http://106.14.66.250:9000/dma/WPG%E6%99%BA%E6%85%A7%E4%BE%9B%E6%B0%B4%E7%AE%A1%E7%90%86%E5%B9%B3%E5%8F%B0%E6%8A%80%E6%9C%AF%E5%9F%B9%E8%AE%AD.pptx"
        },
        {
          title: '二供设备接入平台规范流程',
          hookId: 'pdf',
          url: "http://106.14.66.250:9000/dma/%E4%BA%8C%E4%BE%9B%E8%AE%BE%E5%A4%87%E6%8E%A5%E5%85%A5%E5%B9%B3%E5%8F%B0%E8%A7%84%E8%8C%83%E6%B5%81%E7%A8%8B.pdf"
        },
        {
          title: '繁易Fbox使用流量计算公式',
          hookId: 'pdf',
          url: "http://106.14.66.250:9000/dma/%E7%B9%81%E6%98%93Fbox%E4%BD%BF%E7%94%A8%E6%B5%81%E9%87%8F%E8%AE%A1%E7%AE%97%E5%85%AC%E5%BC%8F.pdf"
        },
      ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.globalData.userInfo
    })
    this.inital();
  },
  inital: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
      that.setData({
        modalName:"Modal",
        neirong: url
      });
      wx.setClipboardData({
        data: url,
        success(res) {
          wx.getClipboardData({
            success(res) {
              console.log(res.data) // data
              wx.showToast({
                title: '复制成功,粘贴到浏览器访问',
                icon: 'none'
              })
            }
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 培训资料',
      path: 'pages/book/book'
    }
  },
})