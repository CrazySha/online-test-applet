// pages/school/aboutus.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    width: '100%',
    height: '',
    version: '',
    QGroupModal: false,
    weappCodeImage: '../../../images/qrcode.png',
    coder: [{
      avatar: 'https://upload-images.jianshu.io/upload_images/4697920-1d2ea2adbeb9f1a0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240',
      nickName: '大笨笨一号'
    }],
    otherApps: [{
      appid: 'wxb7c9e6814c98ac52',
      icon: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/gh1%20(1).jpg?sign=7053cc653543c8c264bcb5674b55e9b8&t=1621169274',
      name: '询价小工具'
    }, {
      appid: 'wx53c26b21015772cc',
      icon: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/gh2%20(1).jpg?sign=6526156ad678c27d4bbeb4865a4ed59f&t=1621169289',
      name: '婚礼邀请函'
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    const accountInfo = wx.getAccountInfoSync();

    that.setData({
      version: accountInfo.miniProgram.version,
      width: wx.getSystemInfoSync().windowWidth * 0.9 + 'px',
      height: wx.getSystemInfoSync().windowWidth * 0.9 * 0.5625 + 'px'
    })
    wx.pageScrollTo({
      scrollTop: 1600,
      duration: 4000,
    })

    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })

    setTimeout(function () {
      wx.pageScrollTo({
        scrollTop: 0,
        duration: 300,
      })
    }, 4000);
  },
  copyID: function () {
    wx.setClipboardData({
      data: 'wxf0ba93e3faff4eda'
    })
    wx.showToast({
      title: '已复制到粘贴版',
      icon: 'none',
      duration: 1000
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '关于 - 【在线考试】 ',
      path: 'pages/my/aboutus/aboutus',
    }
  },
  showAppCode: function () {
    var weappCode = this.data.weappCodeImage;
    wx.previewImage({
      current: weappCode, // 当前显示图片的http链接
      urls: [weappCode] // 需要预览的图片http链接列表
    })
  },
  goOtherApps: function (e) {
    var appid = e.currentTarget.dataset.appid;
    wx.navigateToMiniProgram({ appId: appid })
  },
  checkUpdate: function () {
    wx.navigateTo({
      url: '../exam/index',
    })
  },
  showQQGroupCode: function () {
    this.setData({ QGroupModal: true })
  },
  hideQQGroupCode: function () {
    this.setData({ QGroupModal: false })
  },
  goWeibo: function (e) {
    var weiboPath = e.currentTarget.dataset.weibo;

    if (weiboPath.length > 0) {
      wx.navigateToMiniProgram({
        appId: 'wx9074de28009e1111',
        path: weiboPath
      })
    }
  }
})