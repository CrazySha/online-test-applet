const app = getApp()
const {
  $Toast
} = require('../../../dist/base/index');
const ctx = wx.createCanvasContext('shareCanvas');
var Utils = require('../../../utils/util'); //相对路径
Page({
  data: {
    spinShow: false,
    logoutModalVisible: false,
    info: {},
    count: 0,
    canvasWidth: "",
    canvasHeight: "",
    canvasLeft: "",
    canvasTop: "",
    modalName: null,
    userinfomation: null,
    showhaibao: false, //隐藏显示
    maskHidden: true //隐藏显示
  },

  onLoad: function (options) {
    this.setData({
      spinShow: true,
      info: app.globalData.userInfo
    });
    this.loadUserInfo()
    this.setData({
      spinShow: false
    });
  },
  onReady: function () {

    this.create();
    //创建初始化图片

  },
  hideModal: function () {
    this.setData({
      logoutModalVisible: false
    })
  },
  onShow: function () {
    this.setData({
      maskHidden: true,
      showhaibao: false
    })
  },
  loadUserInfo() {
    let _this = this
    _this.setData({
      spinShow: true
    });
  },
  // 只允许从相机扫码
  subMsg() {
    let that=this;
    wx.scanCode({
      onlyFromCamera: true,
      success: function (res) {
        console.log("扫描了"+res.result)
        try {
          app.globalData.decParam = Utils.decrypt(res.result); //解密
          that.setData({
            modalName: 'Modal',
            userinfomation: app.globalData.decParam
          })
        } catch (err) {
          console.log(err)
          wx.showModal({
            title: '提示',
            content: '非法二维码：校验信息有误！'
          })
        }
      }
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  logOut() {
    let _this = this
    _this.setData({
      spinShow: true
    });
    app.formPost('/api/wx/student/auth/unBind', null).then(res => {
      if (res.code == 1) {
        wx.setStorageSync('token', '')
        wx.setStorageSync('isFirst', false)
        wx.reLaunch({
          url: '/pages/user/bind/index',
        });
      }
      _this.setData({
        spinShow: false
      });
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      app.message(e, 'error')
    })
  },
  showMask: function (e) {
    this.selectComponent("#authorCardId").showMask()
  },
  clickHandler() {
    this.setData({
      count: this.data.count + 1
    });
  },
  clickIng(e) {
    let that = this
    this.zan = setInterval(function () {
      that.setData({
        count: that.data.count + 1
      });
    }, 100)
  },
  clickCancel() {
    clearInterval(this.zan)
  },
  // 震动
  vibrateLongTap: function () {
    // 使手机振动400ms
    wx.vibrateLong();
  },
  vibrateShortTap: function () {
    // 使手机振动15ms
    wx.vibrateShort();
  },
  share: function (e) {
    $Toast({
      content: '图片生成中...',
      type: 'loading',
      duration: 0
    });
    var _this = this
    setTimeout(function () {
      _this.create();
      _this.setData({
        maskHidden: false,
        showhaibao: true
      })
      $Toast.hide();
    }, 2000)
  },
  //创建
  getImage: function (url) {
    return new Promise((resolve, reject) => {
      wx.getImageInfo({
        src: url,
        success: function (res) {
          resolve(res)
        },
        fail: function () {
          reject("")
        }
      })
    })
  },
  getImageAll: function (image_src) {
    let that = this;
    var all = [];
    image_src.map(function (item) {
      all.push(that.getImage(item))
    })
    return Promise.all(all)
  },
  //创建
  create: function () {
    let that = this;
    //图片一把是通过接口请求后台，返回俩点地址，或者网络图片
    // let bg = 'https://www.qinxuewu.club/upload/2018/11/170644l325qooyabhioyaa20181224181300736.jpg';
    // let qr = 'http://www.qinxuewu.club/upload/2018/11/gh_389b881555ad_25820181225234435110.jpg';
    let bg = '../../../images/logo.jpg';
    let qr = '../../../images/gongzhang.png';
    //图片区别下载完成，生成临时路径后，在尽心绘制
    this.getImageAll([bg, qr]).then((res) => {
      let bg = res[0];
      let qr = res[1];
      //设置canvas width height position-left,  为图片宽高
      this.setData({
        canvasWidth: bg.width + 'px',
        canvasHeight: bg.height + 'px',
        canvasLeft: `-${bg.width + 100}px`,
      })
      let ctx = wx.createCanvasContext('canvas');
      ctx.drawImage(bg.path, 0, 0, bg.width, bg.height);
      ctx.drawImage(qr.path, bg.width - qr.width - 100, bg.height - qr.height - 150, qr.width * 0.8, qr.height * 0.8)
      ctx.setFontSize(20)
      ctx.setFillStyle('black')
      ctx.setTextAlign('center')
      ctx.fillText('一款简单的小程序博客', bg.width - qr.width - 1, bg.height - qr.height - 190)
      ctx.draw(that.save());

      // wx.showModal({
      //   title: '提示',
      //   content: '图片绘制完成',
      //   showCancel: false,
      //   confirmText: "点击保存",
      //   success: function () {
      //     that.save()
      //   }
      // })


    })
  },
  //保存
  save: function () {
    var that = this;
    //canvas 生成图片 生成临时路径
    wx.canvasToTempFilePath({
      canvasId: 'canvas',
      success: function (res) {
        console.log(res.tempFilePath);
        var tempFilePath = res.tempFilePath;
        that.setData({
          imagePath: tempFilePath
        });

      }
    })
  },
  gotoSubmit: function (e) {
    $Toast({
      content: '图片生成中...',
      type: 'loading',
      duration: 0
    });
    var _this = this
    setTimeout(function () {
      _this.create();
      _this.setData({
        maskHidden: false,
        showhaibao: true
      })
      $Toast.hide();
    }, 2000)

  },
  //点击图片进行预览，长按保存分享图片
  previewImg: function (e) {
    var img = this.data.imagePath
    wx.previewImage({
      current: img, // 当前显示图片的http链接
      urls: [img] // 需要预览的图片http链接列表
    })
  },
  savelocalImg: function () {
    var _this = this
    wx.saveImageToPhotosAlbum({
      //下载图片
      filePath: _this.data.imagePath,
      success: function () {
        wx.showToast({
          title: "保存成功",
          icon: "success",
        })
        _this.setData({
          maskHidden: false,
          showhaibao: true
        })
      }
    });
  },
  onPullDownRefresh() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    setTimeout(function () {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      //停止当前页面下拉刷新。
      wx.stopPullDownRefresh()
    }, 1000)
  }
})