//index.js
const app = getApp()
var Utils = require('../../../utils/util'); //相对路径
var wxbarcode = require('../../../utils/index');
Page({
  data: {
    info: {},
    spinShow: false,
    coding: ''
  },
  onLoad: function () {
    let _this = this
    _this.setData({
      spinShow: true
    });
    _this.setData({
      spinShow: false,
      coding: app.globalData.userInfo.userLevelName + '--' + app.globalData.userInfo.userDeptName + '--' + app.globalData.userInfo.realName
    });
    console.log(app.globalData.userInfo.userLevelName)

    let erweima = "真实姓名:" + app.globalData.userInfo.realName + ";用户权限:" + app.globalData.userInfo.role+";"+app.globalData.userInfo.userLevelName+"-"+app.globalData.userInfo.userDeptName;
    app.globalData.encParam = Utils.encrypt(erweima); //文件名.方法名  加密
    console.log("加密后"+app.globalData.encParam)
    // app.globalData.decParam  = Utils.decrypt(app.globalData.encParam); //解密
    // console.log("解密后"+app.globalData.decParam)
    wxbarcode.barcode('barcode', app.globalData.encParam, 680, 200);
    wxbarcode.qrcode('qrcode', app.globalData.encParam, 420, 420);
  }
})