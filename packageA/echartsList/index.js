const app = getApp()
import * as echarts from '../ec-canvas/echarts';
let chart = null;
//姓名
let xAxisData = [];
//成绩
let yAxisData = []
function setOption(chart) {
  let option = {
    color: ['#37a2da'],
    tooltip: {
      trigger: 'axis',
      axisPointer: { // 坐标轴指示器，坐标轴触发有效
        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
      },
      confine: true
    },
    legend: {
      data: ['成绩']
    },
    grid: {
      left: 20,
      right: 20,
      bottom: 15,
      top: 40,
      containLabel: true
    },
    xAxis: [{
      type: 'value',
      axisLine: {
        lineStyle: {
          color: '#999'
        }
      },
      axisLabel: {
        color: '#666'
      }
    }],
    yAxis: [{
      type: 'category',
      axisTick: {
        show: false
      },
      data: xAxisData,
      axisLine: {
        lineStyle: {
          color: '#999'
        }
      },
      axisLabel: {
        color: '#666'
      }
    }],
    series: [{
      name: '成绩',
      type: 'bar',
      label: {
        normal: {
          show: true,
          position: 'inside'
        }
      },
      data: yAxisData,
      itemStyle: {}
    }]
  };

}


function initChart(canvas, width, height, dpr) {
  chart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr // new
  });
  var that = this;

    that.setData({
     spinShow: true
   });

    var examPaperId = options.examPaperId
    app.formPost('/api/wx/student/exampaper/answer/rankListOne' + '/' + examPaperId, null).then(res => {
      that.setData({
        spinShow: false,
        detailData: res.response,
      })
      //姓名
       xAxisData = [];
      //成绩
       yAxisData = []
      for (let item of that.data.detailData) {
        let name = 'realName'
        let value = 'userScore'
        xAxisData.push(item[name])
        yAxisData.push(item[value] * 0.1)
      }
      //  console.log(xAxisData)
      //  console.log(yAxisData)
      that.init()

    });
  // canvas.setChart(chart);
  canvas.setChart(chart);
  chart.setOption({
    xAxis: {
      data: xAxisData
    },
    series: {
      data: yAxisData
    }
  })
  
  // setOption(chart);
  // this.chart = chart;
  return chart;
}

Page({
  data: {
    ec: {
      lazyLoad: true,
      onInit: initChart
    },
    spinShow: false,
    detailData: []
  },
  onLoad(options) {

    var that = this;

    that.setData({
     spinShow: true
   });

    var examPaperId = options.examPaperId
    app.formPost('/api/wx/student/exampaper/answer/rankListOne' + '/' + examPaperId, null).then(res => {
      that.setData({
        spinShow: false,
        detailData: res.response,
      })
      //姓名
       xAxisData = [];
      //成绩
       yAxisData = []
      for (let item of that.data.detailData) {
        let name = 'realName'
        let value = 'userScore'
        xAxisData.push(item[name])
        yAxisData.push(item[value] * 0.1)
      }
      //  console.log(xAxisData)
      //  console.log(yAxisData)
      that.init()

    });



  },
  onShareAppMessage: function (res) {},


  onReady() {
    this.ecComponent = this.selectComponent('#mychart-dom-bar');
    setTimeout(function () {
      // 获取 chart 实例的方式
      // this.init_echarts(); //初始化图表
      // console.log(chart)
    }, 2000);
  },
  init() {
    let _this=this
    _this.ecComponent.init((canvas, width, height, dpr) => {
      const chart = echarts.init(canvas, null, {
        width: width,
        height: height,
        devicePixelRatio: dpr // new
      });
      canvas.setChart(chart);
      setOption(chart);
      _this.chart = chart;
      return chart;
    });
  },
})